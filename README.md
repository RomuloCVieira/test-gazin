# Test Gazin Tech

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:
* DOCKER, node 18.13.0, npm 8.19.3


## 🚀 Instalando o projeto:
Para instalar, siga estas etapas:

Navegue até o diretório onde estão localizados seus projetos:
```
cd projects
```
Clone o repositório:
```
git clone https://gitlab.com/RomuloCVieira/test-gazin.git
```
## 🔧 Construindo o projeto:
Navegue até o diretório do projeto:
```
cd test-gazin
```
Suba o container:
```
docker-compose up -d --build
```
### Acesse o host:

http://localhost:4000

### Collections do Postman:
Se encontra dentro do diretório do backend/postman

## ⚙️ Executando os testes:

### coverage:
```
cd reports
```
### tests:
Acesse o container:
```
docker exec -it backend bash
```
Execute os testes e gere o relatório de cobertura:
```
root@b771473ca57b:/var/www/html# vendor/bin/phpunit --coverage-cobertura reports/
```
Saia do container:
```
root@b771473ca57b:/var/www/html# exit;
```

## 📄  documentação da API:

https://app.swaggerhub.com/apis/ROMULOEVIL/test-gazin_tech/1.0.0

## 🎁 Expressões de gratidão

* Agradeço a oportunidade de participar do processo seletivo da Gazin Tech 📢
* Este projeto foi desenvolvido com muito carinho e dedicação, nele foi possível aprender e aprimorar meus conhecimentos em PHP, Node, como CLEAN CODE, SOLID, TDD, DDD, Docker, CLEAN ARCHITECTURE entre outros. 🤓.;

## 🤝 Colaboradores
Colabolador:
<table>
  <tr>
    <td align="center">
      <a href="https://gitlab.com/RomuloCVieira/">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/12269576/avatar.png?width=400" width="100px;" alt="Foto do Iuri Silva no GitHub"/><br>
        <sub>
          <b>Romulo C Vieira</b>
        </sub>
      </a>
    </td>
  </tr>
</table>

