<?php

declare(strict_types=1);

use DI\Container;
use Psr\Container\ContainerInterface;
use Slim\App as AppSlim;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DeveloperController;
use TestGazin\Infrastructure\Http\Controllers\LevelController;
use TestGazin\Infrastructure\Http\Cors;
use TestGazin\Infrastructure\Http\Middlewares\JsonSchemaValidator;
use TestGazin\Infrastructure\Http\Response;
use TestGazin\Infrastructure\Repositories\DeveloperRepository;
use TestGazin\Infrastructure\Repositories\LevelRepository;
use Tuupola\Middleware\CorsMiddleware;

class App
{
    private ContainerInterface $container;
    private AppSlim $app;

    public function __construct()
    {
        $this->container = new Container();
        $this->app = AppFactory::createFromContainer($this->container);

        $this->registerMiddlewares();
        $this->registerRoutes();
        $this->registerServices();
    }

    public function getApp(): AppSlim
    {
        return $this->app;
    }

    private function registerServices(): void
    {
        $this->container->set(DeveloperController::class, function () {
            return new DeveloperController(new DeveloperRepository(
                new MysqlConnection()
            ));
        });

        $this->container->set(LevelController::class, function () {
            return new LevelController(new LevelRepository(
                new MysqlConnection()
            ));
        });
    }

    private function registerRoutes(): void
    {
        $this->app->group('/developers', function (RouteCollectorProxy $group) {
            $group->get('/index', [DeveloperController::class, 'index']);
            $group->get('/show', [DeveloperController::class, 'show']);
            $group->post('/save', [DeveloperController::class, 'save']);
            $group->put('/edit/{id}', [DeveloperController::class, 'edit']);
            $group->delete('/destroy/{id}', [DeveloperController::class, 'destroy']);
        });
        $this->app->group('/levels', function (RouteCollectorProxy $group) {
            $group->get('/index', [LevelController::class, 'index']);
            $group->get('/show', [LevelController::class, 'show']);
            $group->post('/save', [LevelController::class, 'save']);
            $group->put('/edit/{id}', [LevelController::class, 'edit']);
            $group->delete('/destroy/{id}', [LevelController::class, 'destroy']);
        });
    }
    public function registerMiddlewares(): void
    {
        $this->app->add(new JsonSchemaValidator());

        $errorMiddleware = $this->app->addErrorMiddleware(true, true, true);
        $errorMiddleware->setDefaultErrorHandler(function () {
            return Response::internalServerError();
        });

        $this->app->add(new CorsMiddleware(Cors::CONFIG));
    }
}
