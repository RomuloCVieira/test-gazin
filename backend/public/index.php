<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config/App.php';
require __DIR__ . '/../config/donenv.php';

$app = (new App())->getApp();
$app->run();
