<?php

namespace TestGazin\Application;

use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

interface ActionInterface
{
    public function __construct(RepositoryInterface $repository);
    public function action(DTOInterface $dto): array|Level|Developer;
}
