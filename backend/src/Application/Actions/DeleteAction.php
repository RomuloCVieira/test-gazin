<?php

namespace TestGazin\Application\Actions;

use TestGazin\Application\ActionInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class DeleteAction implements ActionInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function action(DTOInterface $dto): array
    {
        $this->repository->delete($dto->id);

        return ['status' => 'deleted'];
    }
}
