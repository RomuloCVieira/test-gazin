<?php

declare(strict_types=1);

namespace TestGazin\Application\Actions\Developer;

use TestGazin\Application\ActionInterface;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class UpdateAction implements ActionInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function action(DTOInterface $dto): Developer
    {
        $developer = Developer::build($dto);
        return $this->repository->edit($developer);
    }
}
