<?php

namespace TestGazin\Application\Actions;

use TestGazin\Application\ActionInterface;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class FindByIdAction implements ActionInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function action(DTOInterface $dto): Level|Developer
    {
        return $this->repository->findById($dto->id);
    }
}
