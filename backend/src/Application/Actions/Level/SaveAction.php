<?php

declare(strict_types=1);

namespace TestGazin\Application\Actions\Level;

use TestGazin\Application\ActionInterface;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class SaveAction implements ActionInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function action(DTOInterface $dto): Level
    {
        $level = Level::build($dto);
        return $this->repository->save($level);
    }
}
