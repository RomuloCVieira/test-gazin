<?php

declare(strict_types=1);

namespace TestGazin\Application\Actions;

use TestGazin\Application\ActionInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class ListAction implements ActionInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function action(DTOInterface $dto): array
    {
        return $this->repository->findAll();
    }
}
