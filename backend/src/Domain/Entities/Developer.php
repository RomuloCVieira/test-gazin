<?php

declare(strict_types=1);

namespace TestGazin\Domain\Entities;

use DateTime;
use InvalidArgumentException;
use TestGazin\Infrastructure\Exceptions\InvalidParameterException;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class Developer
{
    private Level $level;
    private string $name;
    private string $gender;
    private int $age;
    private string $hobby;
    private DateTime $birthdate;
    private ?int $id;

    public function __construct(
        Level $level,
        string $name,
        string $gender,
        DateTime $birthdate,
        string $hobby,
        int $id = null
    ) {
        if (!$this->dateIsValid($birthdate)) {
            throw new InvalidParameterException('date');
        }

        $this->level = $level;
        $this->name = $name;
        $this->gender = $gender;
        $this->hobby = $hobby;
        $this->birthdate = $birthdate;
        $this->age = $this->calculateAge();
        $this->id = $id;
    }

    public static function build(DTOInterface $dto): self
    {
        return new self(
            level: new Level($dto->idlevel),
            name: $dto->name,
            gender: $dto->gender,
            birthdate: new DateTime($dto->birthdate),
            hobby: $dto->hobby,
            id: $dto->id
        );
    }

    public function dateIsValid(DateTime $date): bool
    {
        $now = new DateTime();

        return $date < $now;
    }

    private function calculateAge(): int
    {
        $now = new DateTime();
        $interval = $now->diff($this->birthdate);

        return $interval->y;
    }

    public function getLevel(): Level
    {
        return $this->level;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getHobby(): string
    {
        return $this->hobby;
    }

    public function getBirthdate(): DateTime
    {
        return $this->birthdate;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
