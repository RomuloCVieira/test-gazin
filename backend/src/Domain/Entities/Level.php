<?php

declare(strict_types=1);

namespace TestGazin\Domain\Entities;

use TestGazin\Infrastructure\Http\Controllers\DTOS\DTOInterface;

class Level
{
    private ?int $id;
    private ?string $level;
    private ?int $developers;

    public function __construct(int $id = null, string $level = null, int $developers = null)
    {
        $this->id = $id;
        $this->level = $level;
        $this->developers = $developers;
    }

    public static function build(DTOInterface $dto): self
    {
        return new self(
            id: $dto->id,
            level: $dto->level
        );
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDevelopers(): ?int
    {
        return $this->developers;
    }
}
