<?php

namespace TestGazin\Domain\RepositoriesInterfaces;

use TestGazin\Domain\Entities\Developer;

interface DeveloperRepositoryInterface
{
    public function save(Developer $developer): Developer;
    public function edit(Developer $developer): Developer;
}
