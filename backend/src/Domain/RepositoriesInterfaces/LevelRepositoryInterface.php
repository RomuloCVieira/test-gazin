<?php

namespace TestGazin\Domain\RepositoriesInterfaces;

use TestGazin\Domain\Entities\Level;

interface LevelRepositoryInterface
{
    public function save(Level $level): Level;
    public function edit(Level $level): Level;
}
