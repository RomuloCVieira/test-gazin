<?php

namespace TestGazin\Domain\RepositoriesInterfaces;

use TestGazin\Infrastructure\Database\ConnectionInterface;

interface RepositoryInterface
{
    public function __construct(ConnectionInterface $connection);
    public function findById(int $id): object;
    public function findAll(): array;
    public function delete(int $id): void;
}
