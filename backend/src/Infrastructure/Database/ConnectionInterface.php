<?php

namespace TestGazin\Infrastructure\Database;

use PDO;

interface ConnectionInterface
{
    public static function getInstance(): PDO;
}
