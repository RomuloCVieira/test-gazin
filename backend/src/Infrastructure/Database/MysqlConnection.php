<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Database;

use PDO;

class MysqlConnection implements ConnectionInterface
{
    private static PDO $instance;

    public static function getInstance(): PDO
    {
        $dns = sprintf(
            '%s:host=%s;dbname=%s;charset=utf8',
            $_ENV['DB_DRIVER'],
            $_ENV['DB_HOST'],
            $_ENV['DB_DATABASE']
        );

        if (!isset(self::$instance)) {
            self::$instance = new PDO(
                $dns,
                $_ENV['DB_USERNAME'],
                $_ENV['DB_PASSWORD'],
                [
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ]
            );
        }

        return self::$instance;
    }
}
