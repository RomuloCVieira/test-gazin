-- -----------------------------------------------------
-- Schema gazintech
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gazintech` ;
USE `gazintech`;

-- -----------------------------------------------------
-- Table `gazintech`.`level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gazintech`.`level` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `level` VARCHAR(250) NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `gazintech`.`developer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gazintech`.`developer` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `idlevel` INT NOT NULL,
    `name` VARCHAR(250) NOT NULL,
    `gender` CHAR(1) NOT NULL,
    `birthdate` DATE NOT NULL,
    `age` INT NOT NULL,
    `hobby` VARCHAR(250) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_developer_level_idx` (`idlevel` ASC) VISIBLE,
    CONSTRAINT `fk_developer_level`
    FOREIGN KEY (`idlevel`)
    REFERENCES `gazintech`.`level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

INSERT INTO `gazintech`.`level` (`id`, `level`) VALUES (1, 'Junior A');
INSERT INTO `gazintech`.`level` (`id`, `level`) VALUES (2, 'Junior B');
INSERT INTO `gazintech`.`level` (`id`, `level`) VALUES (3, 'Junior C');

INSERT into `gazintech`.`developer` (`id`, `idlevel`, `name`, `gender`, `birthdate`, `age`, `hobby`) VALUES (1, 1, 'Romulo Vieira', 'M', '1999-06-20', 23, 'Coding');