<?php

namespace TestGazin\Infrastructure\Exceptions;

use Exception;

class IntegrityConstraintViolationException extends Exception
{
}
