<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Exceptions;

use Exception;

class InvalidParameterException extends Exception
{
}
