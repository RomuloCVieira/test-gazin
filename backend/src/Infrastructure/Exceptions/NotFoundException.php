<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Exceptions;

use Exception;

class NotFoundException extends Exception
{
}
