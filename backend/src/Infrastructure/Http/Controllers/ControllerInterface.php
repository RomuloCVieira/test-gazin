<?php

namespace TestGazin\Infrastructure\Http\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;

interface ControllerInterface
{
    public function __construct(RepositoryInterface $repository);
    public function index(): ResponseInterface;
    public function show(Request $request): ResponseInterface;
    public function save(Request $request): ResponseInterface;
    public function edit(Request $request, ResponseInterface $response, array $args): ResponseInterface;
    public function destroy(Request $request, ResponseInterface $response, array $args): ResponseInterface;
}
