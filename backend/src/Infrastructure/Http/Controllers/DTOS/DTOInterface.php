<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Http\Controllers\DTOS;

interface DTOInterface
{
    public function __construct(array $data);
}
