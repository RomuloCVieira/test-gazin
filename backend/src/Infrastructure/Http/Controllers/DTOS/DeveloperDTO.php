<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Http\Controllers\DTOS;

class DeveloperDTO implements DTOInterface
{
    public ?string $name;
    public ?string $gender;
    public ?string $hobby;
    public ?string $birthdate;
    public ?int $idlevel;
    public ?int $id;

    public function __construct(array $developer = [])
    {
        $this->name = $developer['name'] ?? null;
        $this->gender = $developer['gender'] ?? null;
        $this->hobby = $developer['hobby'] ?? null;
        $this->birthdate = $developer['birthdate'] ?? null;
        $this->idlevel = isset($developer['idlevel']) ? ($developer['idlevel']) : null;
        $this->id =  isset($developer['id']) ? (int) $developer['id'] : null;
    }
}
