<?php

namespace TestGazin\Infrastructure\Http\Controllers\DTOS;

class LevelDTO implements DTOInterface
{
    public ?string $level;
    public ?int $id;

    public function __construct(array $levelData = [])
    {
        $this->id = $levelData['id'] ?? null;
        $this->level = $levelData['level'] ?? null;
    }
}
