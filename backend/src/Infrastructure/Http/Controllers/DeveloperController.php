<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Http\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use TestGazin\Application\Actions\DeleteAction;
use TestGazin\Application\Actions\Developer\SaveAction;
use TestGazin\Application\Actions\Developer\UpdateAction;
use TestGazin\Application\Actions\FindByIdAction;
use TestGazin\Application\Actions\ListAction;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Exceptions\IntegrityConstraintViolationException;
use TestGazin\Infrastructure\Exceptions\InvalidParameterException;
use TestGazin\Infrastructure\Exceptions\NotFoundException;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Http\Response;
use TestGazin\Infrastructure\Http\StatusCode;
use TestGazin\Infrastructure\Presenters\DeveloperPresenter;

class DeveloperController implements ControllerInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): ResponseInterface
    {
        $dto = new DeveloperDTO();

        $listAction = new ListAction($this->repository);
        $developers = $listAction->action($dto);

        return Response::success(
            (new DeveloperPresenter())->presentList($developers),
            StatusCode::OK
        );
    }

    public function show(
        Request $request
    ): ResponseInterface {
        try {
            $dto = new DeveloperDTO($request->getQueryParams());

            $findAction = new FindByIdAction($this->repository);
            $developer = $findAction->action($dto);

            return Response::success(
                (new DeveloperPresenter())->present($developer),
                StatusCode::OK
            );
        } catch (NotFoundException $e) {
            return Response::notFound();
        }
    }

    public function save(Request $request): ResponseInterface
    {
        try {
            $body = json_decode($request->getBody()->getContents(), true);

            $dto = new DeveloperDTO($body);

            $saveAction = new SaveAction($this->repository);
            $developer = $saveAction->action($dto);

            return Response::success(
                (new DeveloperPresenter())->present($developer),
                StatusCode::CREATED
            );
        } catch (IntegrityConstraintViolationException $e) {
            return Response::notImplemented($e->getMessage());
        } catch (InvalidParameterException $e) {
            return Response::badRequest('invalid param: ' . $e->getMessage());
        }
    }

    public function edit(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $body = json_decode($request->getBody()->getContents(), true);

        $dto = new DeveloperDTO(array_merge($body, $args));

        $updateAction = new UpdateAction($this->repository);
        $developer = $updateAction->action($dto);

        return Response::success(
            (new DeveloperPresenter())->present($developer),
            StatusCode::OK
        );
    }

    public function destroy(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $dto = new DeveloperDTO($args);

        $deleteAction = new DeleteAction($this->repository);
        $deleteAction->action($dto);

        return Response::success(
            (new DeveloperPresenter())->presentDelete(),
            StatusCode::DELETED
        );
    }
}
