<?php

namespace TestGazin\Infrastructure\Http\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use TestGazin\Application\Actions\DeleteAction;
use TestGazin\Application\Actions\FindByIdAction;
use TestGazin\Application\Actions\Level\SaveAction;
use TestGazin\Application\Actions\Level\UpdateAction;
use TestGazin\Application\Actions\ListAction;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Exceptions\IntegrityConstraintViolationException;
use TestGazin\Infrastructure\Exceptions\NotFoundException;
use TestGazin\Infrastructure\Http\Controllers\DTOS\LevelDTO;
use TestGazin\Infrastructure\Http\Response;
use TestGazin\Infrastructure\Http\StatusCode;
use TestGazin\Infrastructure\Presenters\LevelPresenter;

class LevelController implements ControllerInterface
{
    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): ResponseInterface
    {
        try {
            $dto = new LevelDTO();

            $listAction = new ListAction($this->repository);
            $levels = $listAction->action($dto);

            return Response::success(
                (new LevelPresenter())->presentList($levels),
                StatusCode::OK
            );
        } catch (NotFoundException $e) {
            return Response::notFound();
        }
    }

    public function show(
        Request $request
    ): ResponseInterface {
        try {
            $dto = new LevelDTO($request->getQueryParams());

            $findAction = new FindByIdAction($this->repository);
            $level = $findAction->action($dto);

            return Response::success(
                (new LevelPresenter())->present($level),
                StatusCode::OK
            );
        } catch (NotFoundException $e) {
            return Response::notFound();
        }
    }

    public function save(Request $request): ResponseInterface
    {
        $body = json_decode($request->getBody()->getContents(), true);

        $dto = new LevelDTO($body);

        $saveAction = new SaveAction($this->repository);
        $level = $saveAction->action($dto);

        return Response::success(
            (new LevelPresenter())->present($level),
            StatusCode::CREATED
        );
    }

    public function edit(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $body = json_decode($request->getBody()->getContents(), true);

        $dto = new LevelDTO(array_merge($body, $args));

        $updateAction = new UpdateAction($this->repository);
        $level = $updateAction->action($dto);

        return Response::success(
            (new LevelPresenter())->present($level),
            StatusCode::OK
        );
    }

    public function destroy(Request $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $dto = new LevelDTO($args);

            $deleteAction = new DeleteAction($this->repository);
            $deleteAction->action($dto);

            return Response::success(
                (new LevelPresenter())->presentDelete(),
                StatusCode::DELETED
            );
        } catch (IntegrityConstraintViolationException $e) {
            return Response::notImplemented($e->getMessage());
        }
    }
}
