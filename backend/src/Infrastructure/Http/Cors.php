<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Http;
final class Cors
{
    public const CONFIG = [
        'origin' => ['*'],
        'methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
        'headers.allow' => ["Content-Type"],
        'headers.expose' => [],
        'credentials' => false,
        'cache' => 0,
    ];
}