<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Http\Middlewares;

use League\OpenAPIValidation\PSR7\Exception\NoPath;
use League\OpenAPIValidation\PSR7\Exception\Validation\InvalidBody;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use TestGazin\Infrastructure\Http\Response;

class JsonSchemaValidator implements MiddlewareInterface
{
    public function process(Request $request, RequestHandler $handler): ResponseInterface
    {
        $validator = (new ValidatorBuilder())->fromJsonFile(
            __DIR__ . $_ENV['JSON_SCHEMA_PATH']
        )->getRequestValidator();

        try {
            $validator->validate($request);
        } catch (InvalidBody $invalidBody) {
            return Response::badRequest($invalidBody->getPrevious()->getMessage());
        } catch (NoPath $noPath) {
            return Response::notFound();
        }

        return $handler->handle($request);
    }
}
