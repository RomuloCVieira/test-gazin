<?php

namespace TestGazin\Infrastructure\Http;

use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Response as ResponseSlim;

class Response
{
    public static function success($present, $code = 200): ResponseInterface
    {
        return new ResponseSlim(
            $code,
            new Headers(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream($present)
        );
    }

    public static function badRequest(string $message): ResponseInterface
    {
        return new ResponseSlim(
            StatusCode::BAD_REQUEST,
            new Headers(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream(json_encode(['error' => 'Bad Request', 'message' => $message]))
        );
    }

    public static function notFound(): ResponseInterface
    {
        return new ResponseSlim(
            StatusCode::NOT_FOUND,
            new Headers(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream(json_encode(['error' => 'Not Found']))
        );
    }

    public static function notImplemented(string $message): ResponseInterface
    {
        return new ResponseSlim(
            StatusCode::NOT_IMPLEMENTED,
            new Headers(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream(json_encode(['error' => 'Not Implemented', 'message' => $message]))
        );
    }

    public static function internalServerError(): ResponseInterface
    {
        return new ResponseSlim(
            StatusCode::INTERNAL_SERVER_ERROR,
            new Headers(['Content-Type' => 'application/json']),
            (new StreamFactory())->createStream(json_encode(['error' => 'Internal Server Error']))
        );
    }
}
