<?php

namespace TestGazin\Infrastructure\Http;

class StatusCode
{
    public const OK = 200;
    public const CREATED = 201;
    public const DELETED = 204;
    public const BAD_REQUEST = 400;
    public const NOT_FOUND = 404;
    public const INTERNAL_SERVER_ERROR = 500;
    public const NOT_IMPLEMENTED = 501;
}
