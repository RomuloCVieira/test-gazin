<?php

namespace TestGazin\Infrastructure\Presenters;

use TestGazin\Domain\Entities\Developer;

class DeveloperPresenter implements PresenterInterface
{
    public static function present(object $developer): string
    {
        $prenset = [
            'id' => $developer->getId(),
            'idlevel' => $developer->getLevel()->getId(),
            'name' => $developer->getName(),
            'gender' => $developer->getGender(),
            'birthdate' => $developer->getBirthdate()->format('Y-m-d'),
            'age' => $developer->getAge(),
            'hobby' => $developer->getHobby()
        ];

        if (self::hasLevel($developer)) {
            $prenset['level'] = $developer->getLevel()->getLevel();
        }

        return json_encode($prenset);
    }

    public static function presentList(array $developerList): string
    {

        $developerListPresenter = array_map(function ($developer) {
            /** @var Developer $developer */
            return [
                'id' => $developer->getId(),
                'idlevel' => $developer->getLevel()->getId(),
                'level' => $developer->getLevel()->getLevel(),
                'name' => $developer->getName(),
                'gender' => $developer->getGender(),
                'birthdate' => $developer->getBirthdate()->format('Y-m-d'),
                'age' => $developer->getAge(),
                'hobby' => $developer->getHobby(),
            ];
        }, $developerList);

        return json_encode($developerListPresenter);
    }

    public static function presentDelete(): string
    {
        return json_encode(['message' => 'Developer deleted successfully']);
    }

    public static function hasLevel(object $developer): bool
    {
        return !is_null($developer->getLevel()->getLevel());
    }
}
