<?php

declare(strict_types=1);

namespace TestGazin\Infrastructure\Presenters;

use TestGazin\Domain\Entities\Level;

class LevelPresenter implements PresenterInterface
{
    public static function present(object $level): string
    {
        return json_encode([
            'id' => $level->getId(),
            'level' => $level->getLevel()
        ]);
    }

    public static function presentList(array $levels): string
    {
        if (empty($levels)) {
            return json_encode(['message' => 'No levels found']);
        }

        $arrayPresenter = array_map(function ($level) {
            /** @var Level $level */
            return [
                'id' => $level->getId(),
                'level' => $level->getLevel(),
                'developers' => $level->getDevelopers()
            ];
        }, $levels);

        return json_encode($arrayPresenter);
    }

    public static function presentDelete(): string
    {
        return json_encode(['message' => 'Level deleted successfully']);
    }
}
