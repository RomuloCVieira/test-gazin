<?php

namespace TestGazin\Infrastructure\Presenters;

interface PresenterInterface
{
    public static function present(object $object): string;
    public static function presentList(array $dataList): string;
    public static function presentDelete(): string;
}
