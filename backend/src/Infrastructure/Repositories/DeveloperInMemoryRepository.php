<?php

namespace TestGazin\Infrastructure\Repositories;

use DateTime;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\DeveloperRepositoryInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Database\ConnectionInterface;

class DeveloperInMemoryRepository implements RepositoryInterface, DeveloperRepositoryInterface
{
    private array $list;

    public function __construct(ConnectionInterface $connection)
    {
        $this->list = [
            new Developer(
                level: new Level(
                    1,
                    'Junior A'
                ),
                name: 'Romulo Vieira',
                gender: 'M',
                birthdate: new DateTime('1999-06-20'),
                hobby: 'PlayStation 5',
                id: 1
            ),
            new Developer(
                level: new Level(
                    2,
                    'Junior B'
                ),
                name: 'Joas Vieira',
                gender: 'M',
                birthdate: new DateTime('2002-09-16'),
                hobby: 'PlayStation 2',
                id: 2
            ),
        ];
    }

    public function findAll(): array
    {
        return $this->list;
    }

    public function findById(int $id): Developer
    {
        return $this->list[$id];
    }

    public function save(Developer $developer): Developer
    {
        $developer->setId(count($this->list) + 1);
        $this->list[] = $developer;

        return $developer;
    }

    public function edit(Developer $developer): Developer
    {
        unset($this->list[$developer->getId()]);
        $this->list[$developer->getId()] = $developer;

        return $developer;
    }

    public function delete(int $id): void
    {
        unset($this->list[$id]);
    }
}
