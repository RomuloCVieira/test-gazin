<?php

namespace TestGazin\Infrastructure\Repositories;

use DateTime;
use PDO;
use PDOException;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\DeveloperRepositoryInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Database\ConnectionInterface;
use TestGazin\Infrastructure\Exceptions\IntegrityConstraintViolationException;
use TestGazin\Infrastructure\Exceptions\NotFoundException;

class DeveloperRepository implements RepositoryInterface, DeveloperRepositoryInterface
{
    private const SELECT_ALL_DEVELOPER = 'SELECT d.*, l.level  FROM developer d LEFT JOIN level l ON d.idlevel = l.id';
    private const SELECT_DEVELOPER_PER_ID = 'SELECT d.*, l.level FROM developer d LEFT JOIN level l ON d.idlevel = l.id WHERE d.id = :id';
    private const INSERT_DEVELOPER = 'INSERT INTO developer (idlevel, name, gender, birthdate, age, hobby) VALUES (:idlevel, :name, :gender, :birthdate, :age, :hobby)';
    private const UPDATE_DEVELOPER = 'UPDATE developer SET idlevel = :idlevel, name = :name, gender = :gender, birthdate = :birthdate, age = :age, hobby = :hobby WHERE id = :id';
    private const DELETE_DEVELOPER = 'DELETE FROM developer WHERE id = :id';

    private ConnectionInterface $connection;
    private PDO $instance;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
        $this->instance = $this->connection->getInstance();
    }

    public function findAll(): array
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::SELECT_ALL_DEVELOPER);

        $stmt->execute();

        $this->instance->commit();

        return array_map(function ($developer) {
            return new Developer(
                level: new Level(
                    $developer->idlevel,
                    $developer->level
                ),
                name: $developer->name,
                gender: $developer->gender,
                birthdate: new DateTime($developer->birthdate),
                hobby: $developer->hobby,
                id: $developer->id
            );
        }, $stmt->fetchAll());
    }

    public function findById(int $id): object
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::SELECT_DEVELOPER_PER_ID);
        $stmt->bindValue(':id', $id);

        $stmt->execute();

        $this->instance->commit();

        $result = $stmt->fetchObject();

        if (empty($result)) {
            throw new NotFoundException('Developer not found');
        }

        return new Developer(
            level: new Level(
                $result->idlevel,
                $result->level
            ),
            name: $result->name,
            gender: $result->gender,
            birthdate: new DateTime($result->birthdate),
            hobby: $result->hobby,
            id: $result->id
        );
    }

    public function save(Developer $developer): Developer
    {
        try {
            $this->instance->beginTransaction();

            $stmt = $this->instance->prepare(self::INSERT_DEVELOPER);
            $stmt->bindValue(':idlevel', $developer->getLevel()->getId());
            $stmt->bindValue(':name', $developer->getName());
            $stmt->bindValue(':gender', $developer->getGender());
            $stmt->bindValue(':birthdate', $developer->getBirthdate()->format('Y-m-d'));
            $stmt->bindValue(':age', $developer->getAge());
            $stmt->bindValue(':hobby', $developer->getHobby());

            $stmt->execute();

            $developer->setId($this->instance->lastInsertId());

            $this->instance->commit();

            return $developer;
        } catch (PDOException $e) {
            if ($e->getCode() === '23000') {
                throw new IntegrityConstraintViolationException('Level not found');
            }
        }
    }

    public function edit(Developer $developer): Developer
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::UPDATE_DEVELOPER);
        $stmt->bindValue(':idlevel', $developer->getLevel()->getId());
        $stmt->bindValue(':name', $developer->getName());
        $stmt->bindValue(':gender', $developer->getGender());
        $stmt->bindValue(':birthdate', $developer->getBirthdate()->format('Y-m-d'));
        $stmt->bindValue(':age', $developer->getAge());
        $stmt->bindValue(':hobby', $developer->getHobby());
        $stmt->bindValue(':id', $developer->getId());

        $stmt->execute();

        $this->instance->commit();

        return $developer;
    }

    public function delete(int $id): void
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::DELETE_DEVELOPER);
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $this->instance->commit();
    }
}
