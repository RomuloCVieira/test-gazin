<?php

namespace TestGazin\Infrastructure\Repositories;

use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\LevelRepositoryInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Database\ConnectionInterface;

class LevelInMemoryRepository implements RepositoryInterface, LevelRepositoryInterface
{
    private array $list;

    public function __construct(ConnectionInterface $connection)
    {
        $this->list = [
            new Level(id: 1, level: 'Junior A', developers: 0),
            new Level(id: 2, level: 'Junior B', developers: 5)
        ];
    }

    public function findAll(): array
    {
        return $this->list;
    }

    public function findById(int $id): Level
    {
        return $this->list[$id];
    }

    public function save(Level $level): Level
    {
        $level->setId(count($this->list) + 1);
        $this->list[] = $level;

        return $level;
    }

    public function edit(Level $level): Level
    {
        unset($this->list[$level->getId()]);
        $this->list[$level->getId()] = $level;

        return $level;
    }

    public function delete(int $id): void
    {
        unset($this->list[$id]);
    }
}
