<?php

namespace TestGazin\Infrastructure\Repositories;

use PDO;
use PDOException;
use TestGazin\Domain\Entities\Level;
use TestGazin\Domain\RepositoriesInterfaces\LevelRepositoryInterface;
use TestGazin\Domain\RepositoriesInterfaces\RepositoryInterface;
use TestGazin\Infrastructure\Database\ConnectionInterface;
use TestGazin\Infrastructure\Exceptions\IntegrityConstraintViolationException;
use TestGazin\Infrastructure\Exceptions\NotFoundException;

class LevelRepository implements RepositoryInterface, LevelRepositoryInterface
{
    private const SELECT_ALL_LEVEL = 'SELECT l.*, COUNT(d.id) as developers FROM level l LEFT JOIN developer d on l.id = d.idlevel GROUP BY l.id';
    private const SELECT_LEVEL_PER_ID = 'SELECT * FROM level WHERE id = :id';
    private const INSERT_LEVEL = 'INSERT INTO level (level) VALUES (:level)';
    private const UPDATE_LEVEL = 'UPDATE level SET level = :level WHERE id = :id';
    private const DELETE_LEVEL = 'DELETE FROM level WHERE id = :id';

    private ConnectionInterface $connection;
    private PDO $instance;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
        $this->instance = $this->connection->getInstance();
    }

    public function findAll(): array
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::SELECT_ALL_LEVEL);

        $stmt->execute();

        $this->instance->commit();

        return array_map(function ($level) {
            return new Level(
                id: $level->id,
                level: $level->level,
                developers: $level->developers
            );
        }, $stmt->fetchAll());
    }

    public function findById(int $id): Level
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::SELECT_LEVEL_PER_ID);
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $this->instance->commit();

        $result = $stmt->fetchObject();

        if (empty($result)) {
            throw new NotFoundException('Level not found');
        }

        return new Level(
            id: $result->id,
            level: $result->level
        );
    }

    public function save(Level $level): Level
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::INSERT_LEVEL);
        $stmt->bindValue(':level', $level->getLevel());
        $stmt->execute();

        $level->setId($this->instance->lastInsertId());

        $this->instance->commit();

        return $level;
    }

    public function edit(Level $level): Level
    {
        $this->instance->beginTransaction();

        $stmt = $this->instance->prepare(self::UPDATE_LEVEL);
        $stmt->bindValue('level', $level->getLevel());
        $stmt->bindValue('id', $level->getId());
        $stmt->execute();

        $this->instance->commit();

        return $level;
    }

    public function delete(int $id): void
    {
        try {
            $this->instance->beginTransaction();

            $stmt = $this->instance->prepare(self::DELETE_LEVEL);
            $stmt->bindValue(':id', $id);
            $stmt->execute();

            $this->instance->commit();
        } catch (PDOException $e) {
            if ($e->getCode() === '23000') {
                throw new IntegrityConstraintViolationException('Level is being used');
            }
        }
    }
}
