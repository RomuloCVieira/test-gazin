<?php

namespace Tests\Application\Actions;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\DeleteAction;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;

class DeleteActionTest extends TestCase
{
    public function testShouldDeleteDeveloper()
    {
        $connection = $this->createMock(MysqlConnection::class);
        $repository = new DeveloperInMemoryRepository($connection);
        $deleteAction = new DeleteAction($repository);

        $deleteAction->action(new DeveloperDTO(['id' => 1]));

        $this->assertCount(1, $repository->findAll());
    }

    public function testShouldDeleteLevel()
    {
        $connection = $this->createMock(MysqlConnection::class);
        $repository = new LevelInMemoryRepository($connection);
        $deleteAction = new DeleteAction($repository);

        $deleteAction->action(new DeveloperDTO(['id' => 1]));

        $this->assertCount(1, $repository->findAll());
    }
}