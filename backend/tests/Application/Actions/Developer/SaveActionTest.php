<?php

namespace Tests\Application\Actions\Developer;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\Developer\SaveAction;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;

class SaveActionTest extends TestCase
{
    public function testShouldSaveDeveloper()
    {
        $connection = $this->createMock(MysqlConnection::class);
        $repository = new DeveloperInMemoryRepository($connection);
        $saveAction = new SaveAction($repository);
        $saveAction->action(new DeveloperDTO([
            'idlevel' => 1,
            'name' => 'Romulo Vieira',
            'gender' => 'M',
            'birthdate' => '1990-01-01',
            'hobby' => 'PlayStation 5',
        ]));

        $this->assertCount(3, $repository->findAll());
    }
}