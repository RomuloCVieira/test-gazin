<?php

namespace Tests\Application\Actions\Developer;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\Developer\UpdateAction;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;

class UpdateActionTest extends TestCase
{
    public function testShouldUpdateDeveloper()
    {
        $connection = $this->createMock(MysqlConnection::class);
        $repository = new DeveloperInMemoryRepository($connection);

        $updateAction = new UpdateAction($repository);
        $updateAction->action(new DeveloperDTO([
            'id' => 1,
            'idlevel' => 2,
            'name' => 'Josue Vieira',
            'gender' => 'F',
            'birthdate' => '1978-06-25',
            'hobby' => 'Joga bola'
        ]));

        $developer = $repository->findById(1);

        $this->assertEquals('Josue Vieira', $developer->getName());
        $this->assertEquals(44, $developer->getAge());
    }
}
