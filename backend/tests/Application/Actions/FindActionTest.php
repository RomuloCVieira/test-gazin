<?php

namespace Tests\Application\Actions;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\FindByIdAction;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Infrastructure\Database\ConnectionInterface;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Http\Controllers\DTOS\LevelDTO;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;

class FindActionTest extends TestCase
{
    public function testShouldFindLevel()
    {
        $connectionStub = $this->getConnectionStub();
        $repository = new LevelInMemoryRepository($connectionStub);
        $findAction = new FindByIdAction($repository);

        $level = $findAction->action(new LevelDTO(['id' => 1]));

        $this->assertInstanceOf(Level::class, $level);
        $this->assertEquals('Junior B', $level->getLevel());
    }

    public function testShouldFindDeveloper()
    {
        $connectionStub = $this->getConnectionStub();
        $repository = new DeveloperInMemoryRepository($connectionStub);
        $findAction = new FindByIdAction($repository);

        $developer = $findAction->action(new DeveloperDTO(['id' => 1]));

        $this->assertInstanceOf(Developer::class, $developer);
        $this->assertEquals('Joas Vieira', $developer->getName());
        $this->assertEquals(20, $developer->getAge());
    }


    public function getConnectionStub(): ConnectionInterface
    {
        return $this->createMock(MysqlConnection::class);
    }
}
