<?php

namespace Tests\Application\Actions\Level;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\Level\SaveAction;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\LevelDTO;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;

class SaveActionTest extends TestCase
{
    public function testShouldSaveLevel()
    {
        $connectionStub = $this->createMock(MysqlConnection::class);
        $repository = new LevelInMemoryRepository($connectionStub);
        $saveAction = new SaveAction($repository);
        $saveAction->action(new LevelDTO([
            'id' => 1,
            'level' => 'Senior A'
        ]));

        $this->assertCount(3, $repository->findAll());
    }
}
