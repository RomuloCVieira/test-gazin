<?php

namespace Tests\Application\Actions\Level;

use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\Level\UpdateAction;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\LevelDTO;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;

class UpdateActionTest extends TestCase
{
    public function testShouldUpdateLevel()
    {
        $connectionStub = $this->createMock(MysqlConnection::class);
        $repository = new LevelInMemoryRepository($connectionStub);
        $updateAction = new UpdateAction($repository);
        $updateAction->action(new LevelDTO([
            'id' => 1,
            'level' => 'Junior A'
        ]));

        $level = $repository->findById(1);

        $this->assertEquals('Junior A', $level->getLevel());
    }
}