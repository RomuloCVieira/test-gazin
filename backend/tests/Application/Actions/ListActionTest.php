<?php

namespace Tests\Application\Actions;

use DateTime;
use PHPUnit\Framework\TestCase;
use TestGazin\Application\Actions\ListAction;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Infrastructure\Database\ConnectionInterface;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DTOS\DeveloperDTO;
use TestGazin\Infrastructure\Http\Controllers\DTOS\LevelDTO;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;

class ListActionTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            'developers-data' => [[
                new Developer(
                    new Level(1,
                        'Junior A'
                    ),
                    name: 'Romulo Vieira',
                    gender: 'M',
                    birthdate: new DateTime('1999-06-20'),
                    hobby: 'PlayStation 5',
                    id: 1
                ),
                new Developer(
                    level: new Level(
                        2,
                        'Junior B'
                    ),
                    name: 'Joas Vieira',
                    gender: 'M',
                    birthdate: new DateTime('2002-09-16'),
                    hobby: 'PlayStation 2',
                    id: 2
                ),
            ]]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testShouldListDevelopers(array $developersData): void
    {
        $listAction = new ListAction(new DeveloperInMemoryRepository(
            $this->getConnectionStub()
        ));

        $this->assertEquals($developersData, $listAction->action(new DeveloperDTO()));
    }

    public function testShouldListDevelopersEmpty(): void
    {
        $developersData = [];
        $inMemoryRepositoryStub = $this->createMock(DeveloperInMemoryRepository::class);
        $inMemoryRepositoryStub->method('findAll')->willReturn([]);

        $listAction = new ListAction($inMemoryRepositoryStub);

        $this->assertEquals($developersData, $listAction->action(new DeveloperDTO([])));
    }

    public function testShouldListLevel(): void
    {
        $levelData = [
            new Level(id: 1, level: 'Junior A', developers: 0),
            new Level(id: 2, level: 'Junior B', developers: 5)
        ];

        $listAction = new ListAction(new LevelInMemoryRepository(
            $this->getConnectionStub()
        ));

        $this->assertEquals($levelData, $listAction->action(new LevelDTO()));
    }

    public function testShouldListLevelEmpty(): void
    {
        $developersData = [];
        $inMemoryRepositoryStub = $this->createMock(LevelInMemoryRepository::class);
        $inMemoryRepositoryStub->method('findAll')->willReturn([]);

        $listAction = new ListAction($inMemoryRepositoryStub);

        $this->assertEquals($developersData, $listAction->action(new LevelDTO([])));
    }

    public function getConnectionStub(): ConnectionInterface
    {
        return $this->createMock(MysqlConnection::class);
    }
}
