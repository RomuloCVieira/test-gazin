<?php

namespace Tests\Domain\Entities;

use DateTime;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TestGazin\Domain\Entities\Developer;
use TestGazin\Domain\Entities\Level;
use TestGazin\Infrastructure\Exceptions\InvalidParameterException;

class DeveloperTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            'develop' => [new Developer(
                new Level(
                    1,
                    'junior'
                ),
                name: 'John Doe',
                gender: 'M',
                birthdate: new DateTime('1999-06-20'),
                hobby: 'PlayStation 5'
            )]
        ];
    }

    public function testShouldDateIsInvalid()
    {
        $this->expectException(InvalidParameterException::class);
        $this->expectExceptionMessage('date');

        new Developer(
            level: new Level(
                1,
                'Junior A'
            ),
            name: 'John Doe',
            gender: 'M',
            birthdate: new DateTime('2024-06-20'),
            hobby: 'PlayStation 5'
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAgeCalculationMustBeCorrect(Developer $developer): void
    {
        $this->assertEquals(23, $developer->getAge());
    }
}
