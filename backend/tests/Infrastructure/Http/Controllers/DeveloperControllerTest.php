<?php

namespace Tests\Infrastructure\Http\Controllers;

use Slim\Psr7\Response;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\DeveloperController;
use TestGazin\Infrastructure\Repositories\DeveloperInMemoryRepository;
use Tests\TestCase;

class DeveloperControllerTest extends TestCase
{
    public function testDeveloperListActionResponse(): void
    {
        $expectedBody = json_encode(
            [
                [
                    'id' => 1,
                    'idlevel' => 1,
                    'level' => 'Junior A',
                    'name' => 'Romulo Vieira',
                    'gender' => 'M',
                    'birthdate' => '1999-06-20',
                    'age' => 23,
                    'hobby' => 'PlayStation 5'
                ],
                [
                    'id' => 2,
                    'idlevel' => 2,
                    'level' => 'Junior B',
                    'name' => 'Joas Vieira',
                    'gender' => 'M',
                    'birthdate' => '2002-09-16',
                    'age' => 20,
                    'hobby' => 'PlayStation 2'
                ]
            ]
        );

        $controller = $this->getDeveloperController();

        $request = $this->createRequest(
            method: 'GET',
            path: '/list',
            query: '',
            body: []
        );


        $response = $controller->index();

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeveloperFindActionResponse(): void
    {
        $expectedBody = json_encode([
            'id' => 2,
            'idlevel' => 2,
            'name' => 'Joas Vieira',
            'gender' => 'M',
            'birthdate' => '2002-09-16',
            'age' => 20,
            'hobby' => 'PlayStation 2',
            'level' => 'Junior B'
        ]);

        $controller = $this->getDeveloperController();

        $response = $controller->show(
            $this->createRequest(
                method: 'GET',
                path: '/level/show',
                query: 'id=1',
                body: []
            )
        );

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeveloperSaveActionResponse(): void
    {
        $expectedBody = json_encode([
            'id' => 3,
            'idlevel' => 1,
            'name' => 'Josue Vieira',
            'gender' => 'M',
            'birthdate' => '1978-06-25',
            'age' => 44,
            'hobby' => 'Jogar bola'
        ]);

        $controller = $this->getDeveloperController();

        $request = $this->createRequest(
            method: 'POST',
            path: '/save',
            query: '',
            body: [
                'name' => 'Josue Vieira',
                'gender' => 'M',
                'hobby' => 'Jogar bola',
                'birthdate' => '1978-06-25',
                'idlevel' => 1
            ]
        );


        $response = $controller->save($request);

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testDeveloperUpdateActionResponse(): void
    {
        $extractBody = json_encode([
            'id' => 1,
            'idlevel' => 1,
            'name' => 'Romulo Vieira',
            'gender' => 'M',
            'birthdate' => '1999-06-20',
            'age' => 23,
            'hobby' => 'Play'
        ]);

        $controller = $this->getDeveloperController();

        $request = $this->createRequest(
            method: 'POST',
            path: '/edit',
            query: '',
            body: [
                'id' => 1,
                'name' => 'Romulo Vieira',
                'gender' => 'M',
                'hobby' => 'Play',
                'birthdate' => '1999-06-20',
                'idlevel' => 1,
            ]
        );


        $response = $controller->edit($request, new Response(), ['id' => 1]);

        $this->assertEquals($extractBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testDeveloperDeleteActionResponse(): void
    {
        $expectBody = json_encode([
            'message' => 'Developer deleted successfully'
        ]);

        $controller = $this->getDeveloperController();

        $request = $this->createRequest(
            method: 'DELETE',
            path: '/destroy',
            query: '',
            body: [
                'name' => 'Romulo Vieira',
                'gender' => 'M',
                'hobby' => 'Play',
                'birthdate' => '1999-06-20',
                'level' => 'Junior A',
                'id' => 1
            ]
        );


        $response = $controller->destroy($request, new Response(), ['id' => 1]);

        $this->assertEquals($expectBody, $response->getBody()->getContents());
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function getDeveloperController(): DeveloperController
    {
        $connectionStub = $this->createMock(MysqlConnection::class);
        return new DeveloperController(
            new DeveloperInMemoryRepository($connectionStub)
        );
    }
}
