<?php

namespace Tests\Infrastructure\Http\Controllers;

use Slim\Psr7\Response;
use TestGazin\Infrastructure\Database\MysqlConnection;
use TestGazin\Infrastructure\Http\Controllers\LevelController;
use TestGazin\Infrastructure\Repositories\LevelInMemoryRepository;
use Tests\TestCase;

class LevelControllerTest extends TestCase
{
    public function testLevelListActionResponse(): void
    {
        $expectedBody = json_encode([
            [
                'id' => 1,
                'level' => 'Junior A',
                'developers' => 0
            ],
            [
                'id' => 2,
                'level' => 'Junior B',
                'developers' => 5
            ]
        ]);

        $controller = $this->getLevelController();

        $request = $this->createRequest(
            method: 'GET',
            path: '/list',
            query: '',
            body: []
        );


        $response = $controller->index();

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLevelListActionResponseEmpty(): void
    {
        $expectedBody = json_encode([
            'message' => 'No levels found'
        ]);

        $developerInMemoryRepository = $this->createMock(LevelInMemoryRepository::class);
        $developerInMemoryRepository->method('findAll')->willReturn([]);

        $controller = new LevelController($developerInMemoryRepository);

        $request = $this->createRequest(
            method: 'GET',
            path: '/list',
            query: '',
            body: []
        );

        $response = $controller->index();

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLevelFindActionResponse(): void
    {
        $expectedBody = json_encode([
            'id' => 2,
            'level' => 'Junior B'
        ]);

        $controller = $this->getLevelController();

        $response = $controller->show(
            $this->createRequest(
                method: 'GET',
                path: '/find',
                query: 'id=1',
                body: []
            )
        );

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLevelSaveActionResponse(): void
    {
        $expectedBody = json_encode([
            'id' => 3,
            'level' => 'Junior C'
        ]);

        $controller = $this->getLevelController();

        $request = $this->createRequest(
            method: 'POST',
            path: '/save',
            query: '',
            body: [
                'level' => 'Junior C'
            ]
        );

        $response = $controller->save($request);

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(201, $response->getStatusCode());
    }


    public function testLevelEditActionResponse(): void
    {
        $expectedBody = json_encode([
            'id' => 1,
            'level' => 'Junior C'
        ]);

        $controller = $this->getLevelController();

        $request = $this->createRequest(
            method: 'PUT',
            path: '/edit',
            query: '',
            body: [
                'id' => 1,
                'level' => 'Junior C'
            ]
        );

        $response = $controller->edit($request, new Response(), ['id' => 1]);

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLevelDestroyActionResponse(): void
    {
        $expectedBody = json_encode([
            'message' => 'Level deleted successfully'
        ]);

        $controller = $this->getLevelController();

        $request = $this->createRequest(
            method: 'DELETE',
            path: '/delete',
            query: '',
            body: [
                'id' => 1
            ]
        );

        $response = $controller->destroy($request, new Response(), ['id' => 1]);

        $this->assertEquals($expectedBody, $response->getBody()->getContents());
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function getLevelController(): LevelController
    {
        $connectionStub = $this->createMock(MysqlConnection::class);
        return new LevelController(
            repository: new LevelInMemoryRepository($connectionStub)
        );
    }
}
