import { SidebarWithHeader } from "./components/sidebar-with-header";
import { Routes } from "./routes";

export const App = () => {
  return (
    <SidebarWithHeader>
      <Routes />
    </SidebarWithHeader>
  );
};
