import { Flex, IconButton } from "@chakra-ui/react";
import { FiEdit, FiTrash } from "react-icons/fi";

interface GenericButtonFormProps {
  handleEdit: () => void;
  handleDelete: () => void;
}

export const GenericButtonForm = ({
  handleEdit,
  handleDelete,
}: GenericButtonFormProps) => {
  return (
    <Flex gap={2}>
      <IconButton
        aria-label="edit"
        icon={<FiEdit />}
        background={"blue.300"}
        onClick={handleEdit}
      />
      <IconButton
        aria-label="delete"
        icon={<FiTrash />}
        background={"red.300"}
        onClick={handleDelete}
      />
    </Flex>
  );
};
