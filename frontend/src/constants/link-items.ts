import { IconType } from "react-icons";
import { FiAward, FiUser } from "react-icons/fi";

interface LinkItemProps {
  name: string;
  icon: IconType;
  path: string;
}

export const LinkItems: Array<LinkItemProps> = [
  { name: "Developers", icon: FiUser, path: "/" },
  { name: "Levels", icon: FiAward, path: "/levels" },
];
