export interface DevelopersRequestProps {
  idlevel: number;
  name: string;
  gender: string;
  birthdate: string;
  hobby: string;
}

export interface DevelopersResponseProps {
  id: number;
  idlevel: number;
  level: string;
  name: string;
  gender: string;
  birthdate: string;
  age: number;
  hobby: string;
}
