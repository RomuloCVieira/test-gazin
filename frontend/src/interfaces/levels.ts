export interface LevelsRequestProps {
  level: string;
}

export interface LevelsResponseProps {
  id: number;
  level: string;
  developers: number;
}
