import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { createColumnHelper } from "@tanstack/react-table";
import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GenericButtonForm } from "../../../components/generic-button-form";
import { GenericTable } from "../../../components/generic-table";
import { DevelopersResponseProps } from "../../../interfaces/developers";
import { destroy, findAll } from "../../../services/developers";
import { toastError } from "../../../utils/toast-error";
import { toastSuccess } from "../../../utils/toast-succes";

const columnHelper = createColumnHelper<DevelopersResponseProps>();

export const Table = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = React.useRef() as React.MutableRefObject<HTMLButtonElement>;

  const [id, setId] = useState<number>(0);
  const [developers, setDevelopers] = useState<DevelopersResponseProps[]>([]);

  const columns = [
    columnHelper.accessor("id", {
      id: "id",
      header: () => "Id",
      cell: (info) => info.getValue(),
    }),
    columnHelper.accessor("name", {
      id: "name",
      header: () => "Name",
      cell: (info) => info.renderValue(),
    }),
    columnHelper.accessor("level", {
      id: "level",
      header: () => "Level",
      cell: (info) => {
        return info.getValue();
      },
    }),
    columnHelper.accessor("gender", {
      id: "gender",
      header: () => "Gender",
      cell: (info) => info.renderValue(),
    }),
    columnHelper.accessor((row) => row.birthdate, {
      id: "birthdate",
      header: () => "Birthdate",
      cell: (info) => {
        return moment(info.getValue()).format("DD/MM/YYYY");
      },
    }),
    columnHelper.accessor((row) => row.age, {
      id: "age",
      header: () => "Age",
      cell: (info) => info.renderValue(),
    }),
    columnHelper.accessor((row) => row.hobby, {
      id: "hobby",
      header: () => "Hobby",
      cell: (info) => info.renderValue(),
    }),
    columnHelper.display({
      id: "actions",
      header: () => "Actions",
      cell: ({ row }) => {
        const id = row.original.id;
        return (
          <GenericButtonForm
            handleEdit={() => handleEdit(id)}
            handleDelete={() => {
              onOpen();
              setId(id);
            }}
          />
        );
      },
    }),
  ];

  const findAllDevelopers = () => {
    findAll().then(({ data }) => {
      setDevelopers(data);
    });
  };

  const handleEdit = useCallback((id: number) => {
    navigate(`/developers/${id}`);
  }, []);

  const handleDelete = useCallback((id: number) => {
    destroy(id)
      .then(() => {
        findAllDevelopers();
        toast(toastSuccess("Developer deleted successfully"));
      })
      .catch(({ response }) => {
        toast(toastError(response.data.message));
      })
      .finally(() => {
        onClose();
      });
  }, []);

  useEffect(() => {
    findAllDevelopers();
  }, []);

  return (
    <>
      <GenericTable columns={columns} data={developers} />
      <AlertDialog
        motionPreset="slideInBottom"
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isOpen={isOpen}
        isCentered
      >
        <AlertDialogOverlay />

        <AlertDialogContent>
          <AlertDialogHeader>Delete record?</AlertDialogHeader>
          <AlertDialogCloseButton />
          <AlertDialogBody>
            Do you want to remove this developer??
          </AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              No
            </Button>
            <Button colorScheme="red" ml={3} onClick={() => handleDelete(id)}>
              Yes
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  );
};
