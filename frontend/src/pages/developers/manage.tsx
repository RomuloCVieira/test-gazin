import {
  Box,
  Button,
  ChakraProvider,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Select,
  useToast,
} from "@chakra-ui/react";
import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate, useParams } from "react-router-dom";
import {
  DevelopersRequestProps,
  DevelopersResponseProps,
} from "../../interfaces/developers";
import { LevelsResponseProps } from "../../interfaces/levels";
import { edit, save, show } from "../../services/developers";
import { findAll } from "../../services/levels";
import { toastError } from "../../utils/toast-error";
import { toastSuccess } from "../../utils/toast-succes";
import { validation } from "./schema/validation";

export const Create = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const params = useParams();
  const id = Number(params.id);

  const [developers, setDevelopers] = useState<DevelopersResponseProps>(
    {} as DevelopersResponseProps
  );

  const [levels, setLevels] = useState<LevelsResponseProps[]>([]);

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
    reset,
  } = useForm<DevelopersRequestProps>({
    resolver: yupResolver(validation),
  });

  useEffect(() => {
    if (id) {
      show(id).then(({ data }) => {
        setDevelopers(data);
        reset(developers);
      });
    }
    findAll().then(({ data }) => {
      setLevels(data);
    });
  }, [id]);

  const onSubmit = (developers: DevelopersRequestProps) => {
    if (id) {
      edit(id, developers)
        .then(() => {
          toast(toastSuccess("Developer updated successfully"));
        })
        .catch((error) => {
          toast(toastError(error.response.data.message));
        });
    } else {
      save(developers)
        .then(() => {
          toast(toastSuccess("Developer created successfully"));
        })
        .catch((error) => {
          toast(toastError(error.response.data.message));
        });
    }
    navigate("/");
  };

  return (
    <ChakraProvider>
      <Box p={4}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={!!errors?.name}>
            <FormLabel htmlFor="name">Name</FormLabel>
            <Input
              id="name"
              placeholder="name"
              defaultValue={developers.name}
              {...register("name")}
            />
            <FormErrorMessage>{errors.name?.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={!!errors?.gender}>
            <FormLabel htmlFor="gender">Gender</FormLabel>
            <Select
              placeholder="Selecione..."
              value={developers.gender}
              {...register("gender", {
                onChange: (e) =>
                  setDevelopers({ ...developers, gender: e.target.value }),
              })}
            >
              <option value="M">Masculino</option>
              <option value="F">Feminino</option>
            </Select>
            <FormErrorMessage>{errors.gender?.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={!!errors?.birthdate}>
            <FormLabel htmlFor="birthdate">BirthDate</FormLabel>
            <Input
              id="birthdate"
              type="date"
              placeholder="birthdate"
              defaultValue={developers.birthdate}
              {...register("birthdate")}
            />
            <FormErrorMessage>{errors.birthdate?.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={!!errors?.idlevel}>
            <FormLabel htmlFor="level">Levels</FormLabel>
            <Select
              placeholder="Selecione..."
              value={developers.idlevel}
              {...register("idlevel", {
                onChange: (e) =>
                  setDevelopers({ ...developers, idlevel: e.target.value }),
              })}
            >
              {levels.map(({ id, level }) => (
                <option key={id} value={id}>
                  {level}
                </option>
              ))}
            </Select>
            <FormErrorMessage>{errors.idlevel?.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={!!errors?.hobby}>
            <FormLabel htmlFor="hobby">Hobby</FormLabel>
            <Input
              id="hobby"
              placeholder="hobby"
              defaultValue={developers.hobby}
              {...register("hobby")}
            />
            <FormErrorMessage>{errors.hobby?.message}</FormErrorMessage>
          </FormControl>
          <Link to="/">
            <Button mt={4} mr={2} colorScheme="gray" gap={5}>
              Voltar
            </Button>
          </Link>
          <Button
            mt={4}
            colorScheme="teal"
            isLoading={isSubmitting}
            type="submit"
          >
            Send
          </Button>
        </form>
      </Box>
    </ChakraProvider>
  );
};
