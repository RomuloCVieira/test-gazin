import * as yup from "yup";

export const validation = yup
  .object({
    name: yup.string().required().min(3).max(250),
    gender: yup.string().required().min(1).max(1),
    birthdate: yup.date().required().max(new Date()),
    idlevel: yup.number().positive().integer().required(),
    hobby: yup.string().required().min(3).max(50),
  })
  .required();
