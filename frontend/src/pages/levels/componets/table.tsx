import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { createColumnHelper } from "@tanstack/react-table";
import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GenericButtonForm } from "../../../components/generic-button-form";
import { GenericTable } from "../../../components/generic-table";
import { LevelsResponseProps } from "../../../interfaces/levels";
import { destroy, findAll } from "../../../services/levels";
import { toastError } from "../../../utils/toast-error";
import { toastSuccess } from "../../../utils/toast-succes";

const columnHelper = createColumnHelper<LevelsResponseProps>();

export const Table = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = React.useRef() as React.MutableRefObject<HTMLButtonElement>;

  const [id, setId] = useState<number>(0);
  const [levels, setLevels] = useState<LevelsResponseProps[]>([]);

  const columns = [
    columnHelper.accessor("id", {
      id: "id",
      header: () => "Id",
      cell: (info) => info.getValue(),
    }),
    columnHelper.accessor((row) => row.level, {
      id: "level",
      header: () => "Level",
      cell: (info) => <i>{info.getValue()}</i>,
    }),
    columnHelper.accessor((row) => row.developers, {
      id: "developers",
      header: () => "Developers",
      cell: (info) => <i>{info.getValue()}</i>,
    }),
    columnHelper.display({
      id: "actions",
      header: () => "Actions",
      cell: ({ row }) => {
        const id = row.original.id;

        return (
          <GenericButtonForm
            handleEdit={() => handleEdit(id)}
            handleDelete={() => {
              onOpen();
              setId(id);
            }}
          />
        );
      },
    }),
  ];

  const findAllLevels = () => {
    findAll().then(({ data }) => {
      setLevels(data);
    });
  };

  const handleEdit = useCallback((id: number) => {
    navigate(`/levels/${id}`);
  }, []);

  const handleDelete = useCallback((id: number) => {
    destroy(id)
      .then(() => {
        findAllLevels();
        toast(toastSuccess("Developer deleted successfully"));
      })
      .catch(({ response }) => {
        toast(toastError(response.data.message));
      })
      .finally(() => {
        onClose();
      });
  }, []);

  useEffect(() => findAllLevels, []);

  return (
    <>
      <GenericTable columns={columns} data={levels} />
      <AlertDialog
        motionPreset="slideInBottom"
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isOpen={isOpen}
        isCentered
      >
        <AlertDialogOverlay />

        <AlertDialogContent>
          <AlertDialogHeader>Delete record?</AlertDialogHeader>
          <AlertDialogCloseButton />
          <AlertDialogBody>Do you want to remove this level??</AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              No
            </Button>
            <Button colorScheme="red" ml={3} onClick={() => handleDelete(id)}>
              Yes
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  );
};
