import { Button, Flex } from "@chakra-ui/react";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import { Table } from "./componets/table";

export const Levels = () => {
  return (
    <Flex direction={"column"}>
      <Link
        to="/levels/new"
        style={{
          alignSelf: "flex-end",
        }}
      >
        <Button colorScheme="green" alignItems="center" gap="2">
          <FiPlus />
        </Button>
      </Link>
      <Table />
    </Flex>
  );
};
