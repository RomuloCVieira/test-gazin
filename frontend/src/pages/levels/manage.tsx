import {
  Box,
  Button,
  ChakraProvider,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  useToast,
} from "@chakra-ui/react";
import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate, useParams } from "react-router-dom";
import {
  LevelsRequestProps,
  LevelsResponseProps,
} from "../../interfaces/levels";
import { edit, save, show } from "../../services/levels";
import { toastError } from "../../utils/toast-error";
import { toastSuccess } from "../../utils/toast-succes";
import { validation } from "./schema/validation";

export const Create = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const params = useParams();
  const id = Number(params.id);

  const [levels, setLevel] = useState<LevelsResponseProps>(
    {} as LevelsResponseProps
  );

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
    reset,
  } = useForm<LevelsRequestProps>({
    resolver: yupResolver(validation),
  });

  useEffect(() => {
    if (id) {
      show(id).then(({ data }) => {
        setLevel(data);
        reset(levels);
      });
    }
  }, [id]);

  const onSubmit = (levels: LevelsRequestProps) => {
    if (id) {
      edit(id, levels)
        .then(() => {
          toast(toastSuccess("Level updated successfully"));
        })
        .catch((error) => {
          toast(toastError(error.response.data.message));
        });
    } else {
      save(levels)
        .then(() => {
          toast(toastSuccess("Level created successfully"));
        })
        .catch((error) => {
          toast(toastError(error.response.data.message));
        });
    }
    navigate("/levels");
  };

  return (
    <ChakraProvider>
      <Box p={4}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={!!errors?.level}>
            <FormLabel htmlFor="level">Level</FormLabel>
            <Input
              id="level"
              placeholder="level"
              {...register("level")}
              defaultValue={levels.level}
            />
            <FormErrorMessage>{errors.level?.message}</FormErrorMessage>
          </FormControl>

          <Link
            to="/levels"
            style={{
              alignSelf: "flex-end",
            }}
          >
            <Button mt={4} mr={2} colorScheme="gray" gap={5}>
              Voltar
            </Button>
          </Link>
          <Button
            mt={4}
            colorScheme="teal"
            isLoading={isSubmitting}
            type="submit"
          >
            Send
          </Button>
        </form>
      </Box>
    </ChakraProvider>
  );
};
