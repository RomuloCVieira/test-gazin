import * as yup from "yup";

export const validation = yup
  .object({
    level: yup.string().required().min(1).max(8),
  })
  .required();
