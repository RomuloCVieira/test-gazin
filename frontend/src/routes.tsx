import { Routes as RoutesDom, Route } from "react-router-dom";
import { Developers } from "./pages/developers";
import { Create } from "./pages/developers/manage";
import { Create as CreateLevels } from "./pages/levels/manage";

import { Levels } from "./pages/levels";

export const Routes = () => {
  return (
    <RoutesDom>
      <Route path="/" element={<Developers />} />
      <Route path="/developers/:id" element={<Create />} />
      <Route path="/levels" element={<Levels />} />
      <Route path="/levels/:id" element={<CreateLevels />} />
      <Route path="*" element={<h1>404</h1>} />
    </RoutesDom>
  );
};
