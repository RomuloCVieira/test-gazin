import moment from "moment";
import { DevelopersRequestProps } from "../interfaces/developers";
import { api } from "./index";

export async function findAll() {
  return api.get("/developers/index");
}

export async function save(developers: DevelopersRequestProps) {
  developers.birthdate = moment(developers.birthdate).format("YYYY-MM-DD");
  developers.idlevel = Number(developers.idlevel);
  return api.post("/developers/save", developers);
}

export async function show(id: number) {
  return api.get(`developers/show?id=${id}`);
}

export async function edit(id: number, developers: DevelopersRequestProps) {
  developers.birthdate = moment(developers.birthdate).format("YYYY-MM-DD");
  developers.idlevel = Number(developers.idlevel);
  return api.put(`developers/edit/${id}`, developers);
}

export async function destroy(id: number) {
  return api.delete(`developers/destroy/${id}`);
}
