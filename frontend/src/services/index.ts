import axios from "axios";

export const api = axios.create({
  baseURL: process.env.REACT_APP_HOSTNAME_API,
  timeout: Number(process.env.REACT_APP_TIMEOUT_API),
  headers: { "Content-Type": "application/json" },
});
