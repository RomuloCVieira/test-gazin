import { LevelsRequestProps } from "../interfaces/levels";
import { api } from "./index";

export async function findAll() {
  return api.get("/levels/index");
}

export async function save(level: LevelsRequestProps) {
  return api.post("/levels/save", level);
}

export async function show(id: number) {
  return api.get(`/levels/show?id=${id}`);
}

export async function edit(id: number, level: LevelsRequestProps) {
  return api.put(`/levels/edit/${id}`, level);
}

export async function destroy(id: number) {
  return api.delete(`/levels/destroy/${id}`);
}
